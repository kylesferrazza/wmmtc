---
layout: post
title: "Melodics"
date: 2016-02-22
---
Today we explored [Melodics][melodics], which is a free pad drumming program for Mac and Windows. It can be controlled using finger drumming pads like the [Midifighter][mf] by DJ Techtools or even with your computer keyboard.

[mf]: http://midifighter.com/
[melodics]: http://melodics.com/
