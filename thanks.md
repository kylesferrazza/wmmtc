---
layout: default
title: "Thanks!"
permalink: /contact/thanks/
---
<div class="page">
  <h1 class="page-title">Thank you!</h1>
  <p class="lead">
  	Thank you very much for filling out our contact form! We'll try to get back to you as soon as possible.
  </p>
</div>
