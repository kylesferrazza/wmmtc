---
layout: page
title: About
---
<p>To all students who want to be a part of the hottest rising branch of music today, come and join the new Ward Melville Music Technology Club!</p>

<h1>What is Music Technology?</h1>

<p>Music technology is a newer category in the music family which consists of various topics, such as music production, music mixing and mastering, sound design and theory, and DJ technology and performance. The world of music technology extends from the depths of software coding to live instrument performance and experimental hardware integration and use. Many things done in the music technology world are done using a mixture of these techniques to produce a new product unique to its creator.</p>

<h1>Will I be interested in Music Technology?</h1>

<p>The music technology world is constantly expanding and evolving. Everyday more and more opportunities arise from the music technology genre for people of all interests and passions to experience its unique style and feeling, giving all who have the enthusiasm for music an opportunity to shine in their own way.</p>

<h1>When does the Club meet?</h1>

<p>The music technology club meets on Mondays in the music theory room.</p>

<h1>What will we do in the Club?</h1>

<p>The club as of now will be oriented mostly around music production (with Ableton Live 9 or other software) and DJ mixing and performance (with Serato Pro or Traktor Pro 2). It is very possible that we will also have guest speakers/teachers on other topics and days where we might work on some other aspects of the music technology world. All parts of the music tech genre are connected, so working on many of them at the same time is the best way to gain a higher understanding of it overall.</p>

<h1>Anything else?</h1>

<p>Yes. We hope to gain full support from the school and in turn be able to host events such as school dances where we can raise money and showcase the members’ talents, just like how bands have concerts or things like, for example, Coffeehouse. Also if you are interested in joining, please contact us through our e-mail (<a href="mailto:contact@wmmusictechclub.com">contact@wmmusictechclub.com</a>) so that we can get to you to sign our petition and keep you updated on coming events! Also stay up to date by <a href="https://www.remind.com/join/wmmtc">joining our class on Remind.com</a>.</p>
