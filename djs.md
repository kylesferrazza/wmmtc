---
layout: page
title: DJ Services
---
The Ward Melville Music Technology Club is filled with many young, talented DJs and musicians. One way in which we help these students improve their skills or showcase their abilities is by giving them opportunities in the professional and semi-professional field through our **DJ Services**.

Just as with other DJs, the WMMTC DJ Services will provide the music for your event or party with professional-grade equipment and quality, backed up by an expansive music library. If you wish to contact the WMMTC DJ Services for questions or employment, send us an e-mail at [*dj@wmmusictechclub.com*](mailto:dj@wmmusictechclub.com) and use the subject: **DJ SERVICES**.
