---
layout: page
title: Contact Us
---
You can get hold of us by filling out the form below, <a href="mailto:contact@wmmusictechclub.com">shooting us an email</a>, or by <a href="https://www.remind.com/join/wmmtc">joining our class on Remind.com</a> and starting a new chat. 
<form class="pure-form" action="//formspree.io/wmmtc@kylesferrazza.com" method="POST">
    <fieldset>
        <div class="pure-control-group">
            <label for="name">Name</label><br>
            <input name="name" type="text" placeholder="Your Name"><br><br>
        </div>

        <div class="pure-control-group">
            <label for="email">Email Address</label><br>
            <input name="email" type="email" placeholder="So we can respond"><br><br>
        </div>

        <div class="pure-control-group">
            <label for="message">Questions, Comments, Suggestions</label><br>
            <textarea style="width:100%; height:300px;" name="message" placeholder="Enter something here..."></textarea><br><br>
        </div>

        <input type="hidden" name="_next" value="/contact/thanks"/>

        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary">Send</button>
        </div>
    </fieldset>
</form>
